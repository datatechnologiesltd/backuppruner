//(C) Data Technologies Ltd 2021
//BackupPruner is designed to obtain a list of similar dated file names and delete
//some of them so as to produce a pattern of many recent backups with a less
//and less dense number of files for older dates.
//Deleting backups is a serious business- don't mess it up!!
//The code allows the creation of test data to practice with.
//The code also allows "pure test" runs with no data deletion
program BackupPruner;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  Classes, DateUtils, Math, System.SysUtils;

type
  TDelete=(Nothing, Everything); //"Everything" means only the indicated files

var
  FilePath: String; //Save the file path so we look in the right directory!

//******************************************************************************
function  AddOne(i: Integer):  Integer;
//******************************************************************************
begin
  result:=i+1;
end;

//******************************************************************************
function  FileNameToDate(const FileName: String):  TDate;
//eg suitemaker_live.2020-10-03 00.00.01.zip
//******************************************************************************
var
  posn: Integer;
  datestr: String;
  a, b, c: Integer;
begin
  posn:=pos('2',FileName); //Good for ~1000 years!
  datestr:=copy(FileName,posn,10);
  datestr:=StringReplace(datestr,'-','/', [rfReplaceAll]); //now yyyy/mm/dd
  //Split date into a/b/c
  posn:=pos('/',datestr);
  a:=StrToInt(copy(datestr,1,posn-1));
  datestr:=copy(datestr,posn+1);
  posn:=pos('/',datestr);
  b:=StrToInt(copy(datestr,1,posn-1));
  datestr:=copy(datestr,posn+1);
  c:=StrToInt(datestr);
  if c>31 then  //this is a year
    result:=EncodeDateTime(c,b,a,0,0,0,0)  //Assume AA/BB/CCCC ie DD/MM/YYYY
  else
    result:=EncodeDateTime(a,b,c,0,0,0,0)  //Assume AAAA/BB/CC ie YYYY/MM/DD
end;

//******************************************************************************
procedure RuleA(var FileNames: TStringList);
//Keep the 7 most recent files,
//******************************************************************************
var
  i: Integer;
begin
  FileNames.Sort();  //Sort them alphabetically, which should be in date order, FileNames[0] being oldest
  if FileNames.Count>7 then begin    //Don't bother unless we have at least 7 files
    for i:=FileNames.Count-1 downto Max(0,FileNames.Count-8) do
      FileNames.Delete(i);             //Delete from the list to keep the file
  end;
end;

//******************************************************************************
procedure RuleB(var FileNames: TStringList);
//Keep any files in the last 7 days
//******************************************************************************
var
  i: Integer;
  d: TDate;
begin
    if FileNames.Count>0 then begin    //If we still have files to consider deleting...
    //Keep all files from the last 7 days
    for i:=0 to FileNames.Count-1 do begin
      d:=FileNameToDate(FileNames[i]);
      if d>=now()-7 then           //If less than 7 days old
        FileNames.Delete(i)        //Delete from the list to keep the file
      else
        break;                     //No need to continue after 7 days old
    end;
  end;
end;

//******************************************************************************
procedure RuleC(var FileNames: TStringList);
//Keep any friday in the last 6 weeks
//******************************************************************************
var
  i: Integer;
  d: TDate;
begin
  for i:=FileNames.Count-1 downto 0 do begin
    d:=FileNameToDate(FileNames[i]);
    if (DayOfWeek(d)=6)  and       //Friday and
       (d>now()-42    )  then      //In the last 6 weeks
      FileNames.Delete(i)          //Delete from the list to keep the file
  end;
end;

//******************************************************************************
procedure RuleD(var FileNames: TStringList);
//Keep the first backup in every month for a year
//******************************************************************************
var
  i: Integer;
  d: TDate;
begin
  for i:=FileNames.Count-1 downto 0 do begin
    d:=FileNameToDate(FileNames[i]);
    if (DayOfWeek(d)=6     ) and    //Friday
       (DayOfTheMonth(d)<=7) and    //In the first 7 days of the month
       (d>now()-365        ) then   //Up to a year old
      FileNames.Delete(i);          //Delete from the list to keep the file
  end;
end;

//******************************************************************************
procedure RuleE(var FileNames: TStringList);
//Keep the first backup in every quarter for 2 years
//******************************************************************************
var
  i: Integer;
  d: TDate;
begin
  for i:=FileNames.Count-1 downto 0 do begin
    d:=FileNameToDate(FileNames[i]);
    if (DayOfWeek(d)      = 6) and  //Friday and
       (DayOfTheMonth (d)<= 7) and  //In the first 7 days of the month
      ((MonthOfTheYear(d) = 1) or   //January or
       (MonthOfTheYear(d) = 4) or   //April   or
       (MonthOfTheYear(d) = 7) or   //July    or
       (MonthOfTheYear(d) =10))and  //October
       (d>now()-731          ) then //Up to 2 years old
      FileNames.Delete(i);          //Delete from the list to keep the file
  end;
end;

//******************************************************************************
procedure RuleF(var FileNames: TStringList);
//Keep the first backup in every year forever
//******************************************************************************
var
  i: Integer;
  d: TDate;
begin
  for i:=FileNames.Count-1 downto 0 do begin
    d:=FileNameToDate(FileNames[i]);
    if (DayOfWeek(d)=6     ) and  //Friday and
       (DayOfTheMonth(d)<=7) and  //In the first 7 days of the month and
       (MonthOfTheYear(d)=1) then  //January and
//       (MonthOfTheYear(d)=1)  and  //January and
//       (d>now()-7*365      )  then //Up to 7 years old
      FileNames.Delete(i);        //Delete from the list to keep the file
  end;
end;

//******************************************************************************
procedure RemoveFilesToKeep(FileNames: TStringList);
//Take the list of files found and remove the files that we want to keep
//WARNING: All files left in the list are to be DELETED
//Rules:
//A: Keep the 7 most recent files,
//B: Keep any files in the last 7 days
//C: Keep any friday in the last 6 weeks
//D: Keep the first backup in every month for a year
//E: Keep the first backup in every quarter for 2 years
//F: Keep the first backup in every year for 7 years
//******************************************************************************
begin
  RuleA(FileNames);//A: Keep the 7 most recent files,
  RuleB(FileNames);//B: Keep any files in the last 7 days
  RuleC(FileNames);//C: Keep any friday in the last 6 weeks
  RuleD(FileNames);//D: Keep any Friday in the first 7 days of the month for a year
  RuleE(FileNames);//E: Keep the first backup in every quarter for 2 years
  RuleF(FileNames);//F: Keep the first backup in every year forever
end;

//******************************************************************************
procedure DeleteFiles(FileNames: TStringList; DeleteLevel: TDelete);
//Delete the files with no excuse to live
//DeleteLevel allows us to see which files will be deleted if this were a live run
//******************************************************************************
var
  FileName: String;
begin
  for FileName in FileNames do begin
    if DeleteLevel=Everything then begin
      writeln('Deleting '+FileName);
      DeleteFile(FilePath+FileName);  //Comment this out for testing!!
    end else begin
      writeln('Test mode: NOT Deleting '+FilePath+FileName);
    end;
  end;
end;

//******************************************************************************
procedure FindFiles(FileNames: TStringList; const Match: String; DeleteLevel: TDelete);
//Delete the files with no excuse to live
//******************************************************************************
var
  searchResult: TSearchRec;
begin
  FilePath:=ExtractFilePath(Match); //Save the path to the files.
  FileNames.Clear;
  if FindFirst(Match, faAnyFile, searchResult) = 0 then
  begin
    repeat
      if (searchResult.Attr and faDirectory)<>faDirectory then begin  //We do not want directories
        FileNames.Add(searchResult.Name);
        if DeleteLevel=Nothing then               //Only show for "info only" if no deletion will happen
          WriteLn('Found '+searchResult.Name);
      end;
    until FindNext(searchResult) <> 0;
    FindClose(searchResult);      // Must free up resources used by these successful finds
  end;
end;

//******************************************************************************
procedure CreateFiles(const Match: String);
//Create test files from a pattern like "C:/somepath/suitemaker_live.*.zip"
//******************************************************************************
var
  posn: Integer;
  pre: String;
  post: String;
  i: Integer;
  FileName: String;
  myFile: TextFile;
begin
  posn:=pos('*', Match);  //Find the Asterisk
  assert(posn>0,'File creation requires a * in the pattern, eg Backup*.zip');
  pre:=copy(Match, 1, posn-1);  //Find "C:/somepath/suitemaker_live."
  post:=copy(Match, posn+1);    //Find ".zip"
  assert(pos('*', post)<1,'File creation requires only one * in the pattern');
  WriteLn(GetCurrentDir());
  for i:=0 to 7*365 do begin         //Just create a file
    FileName:=pre+FormatDateTime('yyyy-mm-dd',now()-i)+post;  //Make a new file, with a date
    AssignFile(myFile, FileName);  //Make a new file, with a date
    if FileExists(FileName) then
      Append(myFile)
    else
      ReWrite(myFile);
    CloseFile(myFile);
  end;
end;

//******************************************************************************
function BoolToYN(const b: Boolean):String;
//******************************************************************************
begin
  if b then result:='Y'
  else      result:='N';
end;


//******************************************************************************
  var
    FileNames: TStringList;
    s: String;
    i: Integer; //loop counter
    //Parameter-controlled flags:
    CreateTestFiles: Boolean; //Should we create test files?
    DeleteLevel    : TDelete; //How much data should we destroy?
    ShowHelp       : Boolean; //Should we print the instructions?
    Match          : String ; //What should the target files look like?
begin
//******************************************************************************
  try
    ShowHelp:=false;
    if ParamCount<=0 then  //No parameters means show the help
      ShowHelp:=true;

    CreateTestFiles:=False;
    DeleteLevel    :=Everything;

    Match:='';

    for i:=1 to ParamCount do begin
      s:=ParamStr(i);
      if uppercase(s)='/TEST' then
        DeleteLevel:=Nothing
      else if uppercase(s)='/CREATE' then
        CreateTestFiles:=True
      else if uppercase(s)='/?' then
        ShowHelp:=True
      else if uppercase(s)='/HELP' then
        ShowHelp:=True
      else if pos('*',s)>0 then
        Match:=s
      else begin
        writeln('Sorry, but '+s+' was not recognised. Please try again');
        ShowHelp:=True;
      end;
    end;
    //Check that Match is populated, otherwise show help and exit.
    if Match='' then begin
      ShowHelp:=True;
    end;


    if ShowHelp then begin
      WriteLn('');
      WriteLn('BackupPruner');
      WriteLn('(C) Data Technologies Ltd 2021');
      WriteLn('');
      WriteLn('Designed to delete backups in a controlled way to ensure lots of current');
      WriteLn('backups, but with decreasing frequency as we look back in time.');
      WriteLn('');
      WriteLn('Parameters:');
      WriteLn('You must provide a pattern match to find the backups to delete, eg N:\Data\FTPBackup*Blue.zip');
      WriteLn('We assume the date format is YYYY-MM-DD, eg Backups2021-02-17.tar');
      WriteLn('Later versions, if there are any, might handle these dates better');
      WriteLn('');
      WriteLn('/Create   will make 7 years of daily files for testing with. This should not');
      WriteLn('change any file contents, but don''t use near live data just in case!');
      WriteLn('');
      WriteLn('/Test     will NOT delete anything');
      WriteLn('');
      WriteLn('/? will show this help.');
      WriteLn('');
      WriteLn('Examples:');
      WriteLn('BackupPruner P:\Backups\Save*.CSV                will delete matching backups');
      WriteLn('BackupPruner $HOME\test\B*.X              /test  will not delete anything');
      WriteLn('BackupPruner P:\Backups\Save*.SAV /CREATE /tESt  will make empty files but not delete any');
      WriteLn('BackupPruner $HOME\Store*.Zip     /create        will make empty files and delete the surplus');

      writeln('Press <Enter> to finish...');
      Readln(s);
      exit;
    end;


    //For Development, show the actions expected
    WriteLn('CreateTestFiles='+BoolToYN(CreateTestFiles));
    if DeleteLevel=Nothing then
      WriteLn('DeleteLevel    =Test. NO DELETIONS')
    else
      WriteLn('DeleteLevel    =Everything. REAL DELETIONS');
    WriteLn('Pattern to match: "'+Match+'"');

    //Check that Match is populated, otherwise show help and exit.
    if Match='' then begin
      WriteLn('Please specify which files to delete, eg Backups/Livebackup*.zip');
      exit;
    end;



    { TODO -oUser -cConsole Main : Insert code here }
//    writeln('BackupPruner');

//    writeln(IntToStr(Addone(1)));
    FileNames:=TStringList.Create;
    try
      if CreateTestFiles then
        CreateFiles(Match);
      FindFiles        (FileNames, Match, DeleteLevel);    //Find every file that matches the pattern
      RemoveFilesToKeep(FileNames                    );    //Remove from the list the files we want
      DeleteFiles      (FileNames,        DeleteLevel);    //Delete the files that are left
    finally
      freeandnil(FileNames);
    end;

  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
  writeln('Press <Enter> to finish...');
  readln(s);  //A final "wait" before closing
end.
