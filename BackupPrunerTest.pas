unit BackupPrunerTest;

interface
uses
  DUnitX.TestFramework;

type

  [TestFixture]
  TMyTestObject = class(TObject) 
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    // Sample Methods
    // Simple single Test
    [Test]
    procedure Test1;
    // Test with TestCase Attribute to supply parameters.
    [Test]
    [TestCase('TestA','1,2')]
    [TestCase('TestB','3,4')]
    procedure Test2(const AValue1 : Integer;const AValue2 : Integer);
  end;

implementation

//******************************************************************************
procedure TMyTestObject.Setup;
//******************************************************************************
var
  s: String;
begin
  //ParamCount
  writeln('CHEESECAKE');
  writeln(ParamStr(0));
  readln(s);
end;

//******************************************************************************
procedure TMyTestObject.TearDown;
//******************************************************************************
begin
end;

//******************************************************************************
procedure TMyTestObject.Test1;
//******************************************************************************
begin
//  AllocConsole;
  //OutputDebugString('fred');
  writeln('CHEESECAKE');
  writeln(ParamStr(0));
  Setup();
end;

//******************************************************************************
procedure TMyTestObject.Test2(const AValue1 : Integer;const AValue2 : Integer);
//******************************************************************************
begin
end;

initialization
  TDUnitX.RegisterTestFixture(TMyTestObject);
end.
